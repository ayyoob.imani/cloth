package ir.hisis.cloth;

import ir.hisis.cloth.Utils.Utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.Inflater;

import com.etsy.android.grid.StaggeredGridView;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	
	
	public static String TAG = MainActivity.class.toString();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		if(!ir.hisis.cloth.Utils.UtilUser.isAuthenticated(getApplication())){
			Intent regi = new Intent(this,RegisterActivity.class);
			startActivity(regi);
			finish();
		}
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new mainViewFragment()).commit();
		}
		
		
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
			case(R.id.action_explore):
				Intent intent = new Intent(getApplicationContext(), ExploreActivity.class);
				startActivity(intent);
				return true;
			case(R.id.action_settings):
				Intent intent2 = new Intent(getApplicationContext(), CategoryActivity.class);
				startActivity(intent2);
				return true;
			}
		
		return super.onOptionsItemSelected(item);
		 
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class mainViewFragment extends Fragment {

		public mainViewFragment() {
			
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			
			
			return rootView;
		}
		
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
			StaggeredGridView gridView = (StaggeredGridView) getView().findViewById(R.id.grid_view);
			Context context = getActivity().getApplicationContext();
			gridView.setAdapter(new ImageAdapter(context));
			
		}
		
		public class ImageAdapter extends BaseAdapter {
		    private Context mContext;
		    public String TAG = "base adapter";
		    public ImageAdapter(Context c) {
		        mContext = c;
		    }

		    public int getCount() {
		        return mThumbIds.length;
		    }

		    public Object getItem(int position) {
		        return null;
		    }

		    public long getItemId(int position) {
		        return 0;
		    }

		    // create a new ImageView for each item referenced by the Adapter
		    public View getView(int position, View convertView, ViewGroup parent) {
		        View imageItem;
		        if (convertView == null) {
		        	LayoutInflater inflater= getActivity().getLayoutInflater();
		        	imageItem = inflater.inflate(R.layout.image_item, null);
		        	
		        	ImageItemViewHolder tag = new ImageItemViewHolder();
		        	tag.image = (ImageView) imageItem.findViewById(R.id.image);
		        	tag.tagsContainer = (LinearLayout) imageItem.findViewById(R.id.tags_container);
		        	tag.likeImage = (ImageView) imageItem.findViewById(R.id.like_image);
		        	setShareLongClickListenerForImage(tag.image);
		        	
		        	imageItem.setTag(tag);
		        } else {
		            imageItem =  convertView;
		        }
		        
		        ImageItemViewHolder tag = (ImageItemViewHolder) imageItem.getTag();
		        ImageView img = tag.image;
		        img.setImageResource(mThumbIds[position]);
		        
		        return imageItem;
		    }
		    
		    private void setShareLongClickListenerForImage(ImageView image){
		    	image.setOnLongClickListener(new OnLongClickListener() {
					
					@Override
					public boolean onLongClick(View arg0) {
						try{
							Log.wtf(TAG, "starting to share image");
							Utils.shareImage((ImageView) arg0, getActivity());
						}
						catch(FileNotFoundException e){
							Toast.makeText(arg0.getContext(), getString(R.string.external_storage_is_uavailable), Toast.LENGTH_LONG).show();
						}catch (IOException e) {
							Toast.makeText(arg0.getContext(), getString(R.string.proble_with_external_storage), Toast.LENGTH_LONG).show();
						}
						
						return true;
					}
				});
		    }
		    
		    
		    // references to our images
		    private Integer[] mThumbIds = {
		    		R.drawable.logo_default,
		            R.drawable.sample_2, R.drawable.sample_3,
		            R.drawable.sample_4, R.drawable.sample_5,
		            R.drawable.sample_6, R.drawable.sample_7,
		            R.drawable.sample_0, R.drawable.sample_1,
		            R.drawable.sample_2, R.drawable.sample_3,
		            R.drawable.sample_4, R.drawable.sample_5,
		            R.drawable.sample_6, R.drawable.sample_7,
		            R.drawable.sample_0, R.drawable.sample_1,
		            R.drawable.sample_2, R.drawable.sample_3,
		            R.drawable.sample_4, R.drawable.sample_5,
		            R.drawable.sample_6, R.drawable.sample_7
		            
		    };
		}
		
		public static class ImageItemViewHolder{
			public ImageView image;
			public LinearLayout tagsContainer;
			public ImageView likeImage;
		}

	}
	
	
}
