package ir.hisis.cloth.Utils;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class UtilUser {
	public static String USER_NAME_LABEL = "user_name";
	public static String PASSWORD_LABEL = "password";
	public static String AUTHENTICATIONFILE = "authentication";
	
	public static class PasswordMatchException extends Exception{
	}
	
	public static boolean isAuthenticated(Application application){
		Map<String, String> authToken = getAuthToken(application);
		if(authToken.get(PASSWORD_LABEL) == null || authToken.get(USER_NAME_LABEL) == null)
			return false;
		else
			return true;
	}
	
	public static void registerUser(String userName, String passwd, String repeatPass, Application application)throws PasswordMatchException{
		if(!passwd.equals(repeatPass)) throw new PasswordMatchException();
		SharedPreferences sharedPreferences =  application.getSharedPreferences(AUTHENTICATIONFILE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(USER_NAME_LABEL, userName);
		editor.putString(PASSWORD_LABEL, passwd);
		editor.commit();
	}
	
	public static boolean authenticate(String userName, String passwd, Application application){
		Map<String, String> token = getAuthToken(application);
		if(userName.equals(token.get(USER_NAME_LABEL)) && passwd.equals(token.get(PASSWORD_LABEL)) )
				return true;
		return false;
	}
	
	public static Map<String, String> getAuthToken(Application application){
		SharedPreferences sharedPreferences = application.getSharedPreferences(AUTHENTICATIONFILE, Context.MODE_PRIVATE);
		Map<String, String> token  = new HashMap<String, String>();
		token.put(USER_NAME_LABEL, sharedPreferences.getString(USER_NAME_LABEL, null));
		token.put(PASSWORD_LABEL, sharedPreferences.getString(PASSWORD_LABEL, null));
		return token;
	}
	
}
