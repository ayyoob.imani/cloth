package ir.hisis.cloth;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.database.MatrixCursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.SearchView;

import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;


public class ExploreFragment extends Fragment {
	
	View view;
	private Menu menu;
	private FragmentActivity activity;
	private ExpandableListView listView;
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		view = inflater.inflate(R.layout.explore_list, container, false);
		
		Display newDisplay = getActivity().getWindowManager().getDefaultDisplay(); 
		int width = newDisplay.getWidth();
		listView = (ExpandableListView) view.findViewById(R.id.lvExplore);
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			listView.setIndicatorBounds(width-50, width);
		}else{
			listView.setIndicatorBoundsRelative(width-50, width);
		}
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);
		
		activity = getActivity();
		ExploreListAdapter listAdapter = new ExploreListAdapter(activity);
		listView.setAdapter(listAdapter);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.search:
			activity.onSearchRequested();
			return true;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	

	@SuppressLint("NewApi")
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

	    inflater.inflate(R.menu.explore, menu);

	    this.menu = menu;

	    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	    	//check this url out : http://wptrafficanalyzer.in/blog/android-searchview-widget-with-actionbarcompat-library/
	        SearchManager manager = (SearchManager) activity.getSystemService(Context.SEARCH_SERVICE);
	        SearchView search = (SearchView) menu.findItem(R.id.search).getActionView();
	        search.setSearchableInfo(manager.getSearchableInfo(activity.getComponentName()));
	    }

	    

	}

}
